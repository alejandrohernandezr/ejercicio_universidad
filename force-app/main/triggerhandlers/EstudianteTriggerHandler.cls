/**
 * @description       : 
 * @last modified on  : 10-26-2021
 * @last modified by  : Alejandro Hernández
**/
public with sharing class EstudianteTriggerHandler {
    
    // ¿Cómo hacerlo? ¿Pasas id y haces una query dentro?

    // @Future(callout=true)
    // private static void crearAdmisionAsociadaAlNuevoEstudiante(String emailEstudiante, Map<String, Contact> estudiantes) {
    //     Opportunity admision = new Opportunity();
    //     Contact estudiante = estudiantes.get(emailEstudiante);
    private static void crearAdmisionAsociadaAlNuevoEstudiante(Contact estudiante) {
        Opportunity admision = new Opportunity();

        if (estudiante != null) {
            admision.ContactId = estudiante.Id;
            admision.StageName = AdmisionTriggerHandler.INTERESADO;
            admision.Name = 'Admisión ' + estudiante.FirstName + ' ' + estudiante.LastName;
            admision.CloseDate = Date.today();
            admision.AccountId = estudiante.AccountId;
            admision.Via_acceso__c = estudiante.Via_acceso__c;
            admision.Categoria__c = 'Grado';

            insert admision;
        }
    }

    public static void comprobarSiExisteEstudianteYCrearAdmisionRelacionada(Contact estudiante, Map<String, Contact> estudiantes) {
        Contact estudianteB = null;

        if (estudiantes.containsKey(estudiante.Email)) {
            estudianteB = estudiantes.get(estudiante.Email);
            estudiante.addError('Ya existe un estudiante con ese email. Por favor, cierre esta ventana. Se ha creado una nueva admisión para el contacto ya existente.');
            EstudianteTriggerHandler.crearAdmisionAsociadaAlNuevoEstudiante(estudianteB);
        } else {
            EstudianteTriggerHandler.crearAdmisionAsociadaAlNuevoEstudiante(estudiante);
        }
    }
}

