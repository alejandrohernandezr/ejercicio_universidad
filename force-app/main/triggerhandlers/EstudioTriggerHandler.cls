/**
 * @description       : 
 * @last modified on  : 10-27-2021
 * @last modified by  : Alejandro Hernández
**/
public with sharing class EstudioTriggerHandler {

    @future(callout=true)
    public static void actualizarValueSetGlobalConNuevoEstudio() {
        List<OM_GlobalValueSetUtils.CustomValue> nuevosValoresSet = new List<OM_GlobalValueSetUtils.CustomValue>();

        for (Account acc : [SELECT Name, Codigo__c FROM Account WHERE Name != null AND Codigo__c != null]) {
            nuevosValoresSet.add(new OM_GlobalValueSetUtils.CustomValue(acc.Name, acc.Codigo__c, true));
        }
        
        OM_GlobalValueSetUtils.setNewValues('Nombre_carrera', 'Nombre_carrera', false, nuevosValoresSet);
    }
}