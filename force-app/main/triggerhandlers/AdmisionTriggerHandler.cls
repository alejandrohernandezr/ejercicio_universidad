/**
 * @description       : 
 * @last modified on  : 10-27-2021
 * @last modified by  : Alejandro Hernández
**/
public with sharing class AdmisionTriggerHandler {
    public static final String INTERESADO = 'Interesado';
    public static final String PREADMISION = 'Pre-admisión';
    public static final String ADMITIDO = 'Admisión';
    public static final String MATRICULADO = 'Matriculado';
    public static final String PENDIENTE = 'Pendiente entrega';
    public static final String ENTREGADO = 'Entregado';
    public static final String VALIDADO = 'Validado';
    public static final String RECHAZADO = 'Matriculado';

    public AdmisionTriggerHandler() {

    }

    public static void generarSolicitudDocumentacion(Opportunity admision, Set<Maestro_Documentacion__c> maestros, Map<Id, Account> estudios) {
        System.debug('##################### estudios ##############' + JSON.serializePretty(estudios));

        for (Maestro_Documentacion__c maestro : maestros) {
            System.debug('###############################################################'+JSON.serializePretty(maestro));
            String nombreEstudio = estudios.get(admision.AccountId).Codigo__c; 

            System.debug('M: '+maestro.Via_acceso__c.split(';') + ' --- '+admision.Via_acceso__c);
            System.debug('M: '+maestro.Estudio__c.split(';') + ' --- ' + nombreEstudio);
            if ((maestro.Via_acceso__c.split(';').contains(admision.Via_acceso__c)) && (maestro.Estudio__c.split(';').contains(nombreEstudio))) {
                Solicitud_Documentacion__c solicitud = new Solicitud_Documentacion__c();
                solicitud.Admision__c = admision.Id;
                solicitud.Estado__c = PENDIENTE;
                solicitud.Maestro_Documentacion__c = maestro.Id;
                
                insert solicitud;
            }
        }
    }

    public static void asignarAgrupacionAdmisiones(Opportunity admision, Map<String, Id> agrupaciones) {
        Id idAgrupacion = agrupaciones.get(admision.Categoria__c);
        admision.Agrupacion_admisiones__c = idAgrupacion;
    }
}

