/**
 * @description       : 
 * @last modified on  : 10-27-2021
 * @last modified by  : Alejandro Hernández
**/
public class OM_GlobalValueSetUtils {

	public class MetadataWrapper {
		public Metadata Metadata;
		public String FullName;

        public MetadataWrapper(){}
	}

	public class Metadata {
		public List<CustomValue> customValue {get;set;} 
		public Object description {get;set;} 
		public String masterLabel {get;set;} 
		public Boolean sorted {get;set;} 
		public Object urls {get;set;} 

        public Metadata(){}
	}


	public class CustomValue {
		public Object color {get;set;} 
		//public Boolean default_Z {get;set;} 
		public Object description {get;set;} 
		public Boolean isActive {get;set;} 
		public String label {get;set;} 
		public Object urls {get;set;} 
		public String valueName {get;set;}
        
        public CustomValue(String value, String label, Boolean isActive){
            this.valueName = label;
            this.label = value;
            this.isActive = isActive;
            this.description = null;
            this.urls = null;
        }
	}
	

	public static String getGlobalValueSetId(String globalValueSetName){

		HttpRequest req = new HttpRequest();
        if(Test.isRunningTest()){
            req.setHeader('Authorization', 'getId');            
        }else req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());

		req.setHeader('Content-Type', 'application/json');      
		req.setEndpoint(URL.getSalesforceBaseUrl().toExternalForm()+'/services/data/v45.0/tooling/query/?q=select+id+from+GlobalValueSet+where+masterLabel=\''+globalValueSetName+'\'+limit+1');
		req.setMethod('GET');
		Http httpreq = new Http();
		HttpResponse res  = httpreq.send(req);
        if(res.getStatusCode()==200){
        	OM_GlobalValueSetResponse globalValueSetResp = OM_GlobalValueSetResponse.parse(res.getBody());    
            return globalValueSetResp.records[0].Id;
        }else{
            return null;
        }
    }

	public static Boolean setNewValues(String label, String name, Boolean sorted, List<CustomValue> values){

        String globalValueSetId = OM_GlobalValueSetUtils.getGlobalValueSetId(label);
        
        if(globalValueSetId!=null){
            
        	Metadata mt = new Metadata();
            mt.masterLabel = label;
            mt.sorted = sorted;
            mt.customValue = values;
            
            MetadataWrapper mw = new MetadataWrapper();
            mw.Metadata = mt;
            mw.FullName = name;
            
            String body = JSON.serialize(mw);
            
            HttpRequest req = new HttpRequest();
            req.setBody(body);
            if(Test.isRunningTest()){
                req.setHeader('Authorization', 'setGlobalValueSet');            
            }else req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());

            req.setHeader('Content-Type', 'application/json');      
            req.setEndpoint(URL.getSalesforceBaseUrl().toExternalForm()+'/services/data/v45.0/tooling/sobjects/GlobalValueSet/' + globalValueSetId + '?_HttpMethod=PATCH');
            req.setMethod('POST');

            Http httpreq = new Http();
            HttpResponse res  = httpreq.send(req);
            
            if(res.getStatusCode()==200 || res.getStatusCode()==204){
                return true;
            }else{
                return false;
            }
            
        }
        else return false;
		
	}
	

}