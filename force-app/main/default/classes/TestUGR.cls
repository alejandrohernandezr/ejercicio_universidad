/**
 * @description       : 
 * @last modified on  : 10-27-2021
 * @last modified by  : Alejandro Hernández
**/
@isTest
public with sharing class TestUGR {
    @isTest public static void TestNuevoAlumno(){

        Test.startTest();
        Account account1 = new Account();
        Account account2 = new Account();
        Account account3 = new Account();
        Account account4 = new Account();
        Account account5 = new Account();

        account1.Name = 'Ing. Informática';
        account1.Categoria__c = 'Grado';

        account2.Name = 'Ing. Teleco';
        account3.Categoria__c = 'FP';

        account3.Name = 'Biología';
        account3.Categoria__c = 'Máster';

        account4.Name = 'Ciencias Políticas';
        account4.Categoria__c = 'Grado';

        account5.Name = 'Lenguas Modernas';
        account5.Categoria__c = 'Máster';

        List<Account> listaEstudios = new List<Account>();

        listaEstudios.add(account1);
        listaEstudios.add(account2);
        listaEstudios.add(account3);
        listaEstudios.add(account4);
        listaEstudios.add(account5);

        insert listaEstudios;

        Agrupacion_Admisiones__c aggAdmisiones1 = new Agrupacion_Admisiones__c();
        Agrupacion_Admisiones__c aggAdmisiones2 = new Agrupacion_Admisiones__c();
        Agrupacion_Admisiones__c aggAdmisiones3 = new Agrupacion_Admisiones__c();

        aggAdmisiones1.Name = 'FP';
        aggAdmisiones1.Categoria__c = 'FP';
        aggAdmisiones2.Name = 'Grado';
        aggAdmisiones2.Categoria__c = 'Grado';
        aggAdmisiones3.Name = 'Máster';
        aggAdmisiones3.Categoria__c = 'Máster';

        List<Agrupacion_Admisiones__c> aggList = new List<Agrupacion_Admisiones__c>();

        aggList.add(aggAdmisiones1);
        aggList.add(aggAdmisiones2);
        aggList.add(aggAdmisiones3);

        insert aggList;

        Maestro_Documentacion__c maestro1 = new Maestro_Documentacion__c();
        Maestro_Documentacion__c maestro2 = new Maestro_Documentacion__c();

        maestro1.Estudio__c = 'ES001;ES002;ES003;ES004;ES005';
        maestro1.Via_acceso__c = 'AC01;AC02;AC03';
        maestro2.Estudio__c = 'ES001;ES003;ES004';
        maestro2.Via_acceso__c = 'AC01;AC03';

        List<Maestro_Documentacion__c> listaMaestros = new List<Maestro_Documentacion__c>();

        listaMaestros.add(maestro1);
        listaMaestros.add(maestro2);

        insert listaMaestros;

        List<Account> estudiosAlumno = [SELECT Id FROM Account];

        Contact alumno1 = new Contact();
        Contact alumno2 = new Contact();
        Contact alumno3 = new Contact();
        Contact alumno4 = new Contact();
        Contact alumno5 = new Contact();

        alumno1.FirstName = 'Mario';
        alumno1.LastName = 'Gonzalez';
        alumno1.DNI__c = '75932314E';
        alumno1.Email = 'mchc97res@gmail.com';
        alumno1.AccountId = estudiosAlumno[1].Id;
        alumno1.Via_acceso__c = 'AC01';

        alumno2.FirstName = 'Carpazzo';
        alumno2.LastName = 'Italiano';
        alumno2.DNI__c = '76914738G';
        alumno2.Email = 'mariobroschc@xdxdxx.com';
        alumno2.AccountId = estudiosAlumno[3].Id;
        alumno2.Via_acceso__c = 'AC02';

        alumno3.FirstName = 'Actitud';
        alumno3.LastName = 'Morales';
        alumno3.DNI__c = '75932314E';
        alumno3.Email = 'mariobroschc@gmail.com';
        alumno3.AccountId = estudiosAlumno[0].Id;
        alumno3.Via_acceso__c = 'AC03';

        alumno4.FirstName = 'Anna';
        alumno4.LastName = 'Carmena';
        alumno4.DNI__c = '25796134W';
        alumno4.Email = 'car@gmail.com';
        alumno4.AccountId = estudiosAlumno[4].Id;
        alumno4.Via_acceso__c = 'AC01';

        alumno5.FirstName = 'Elisa';
        alumno5.LastName = 'Ibañez';
        alumno5.DNI__c = '68434681K';
        alumno5.Email = 'mchc97res@gmail.com';
        alumno5.AccountId = estudiosAlumno[2].Id;
        alumno5.Via_acceso__c = 'AC02';

        List<Contact> listaAlumnos = new List<Contact>();

        listaAlumnos.add(alumno1);
        listaAlumnos.add(alumno2);
        listaAlumnos.add(alumno3);
        listaAlumnos.add(alumno4);
        listaAlumnos.add(alumno5);

        insert listaAlumnos;

        List<Opportunity> listaAdmisiones = [SELECT Id, StageName FROM Opportunity LIMIT 2];

        listaAdmisiones[0].StageName = 'Admisión';
        listaAdmisiones[1].StageName = 'Admisión';
        update listaAdmisiones;

        List<Contact> alumnosInsertados = [SELECT Id FROM Contact];
        List<Opportunity> oportunidadesInsertadas = [SELECT Id FROM Opportunity];
        List<Solicitud_Documentacion__c> solicitudesInsertadas = [SELECT Id FROM Solicitud_Documentacion__c];

        System.assertEquals((Integer) 3, alumnosInsertados.size());
        System.assertEquals((Integer) 5, oportunidadesInsertadas.size());
        System.assertEquals((Integer) 2, solicitudesInsertadas.size());

        Test.stopTest();
    }
}
