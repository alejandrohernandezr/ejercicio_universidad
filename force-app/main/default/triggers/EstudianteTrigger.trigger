/**
 * @description       : 
 * @last modified on  : 10-26-2021
 * @last modified by  : Alejandro Hernández
**/
trigger EstudianteTrigger on Contact (before delete, before insert, before update, after delete, after insert, after update) {

    Map<String, Contact> estudiantes = new Map<String, Contact>();
    /*
     _         _ _     _          __            
    | |__ _  _| | |__ | |__  ___ / _|___ _ _ ___
    | '_ \ || | | / / | '_ \/ -_)  _/ _ \ '_/ -_)
    |_.__/\_,_|_|_\_\ |_.__/\___|_| \___/_| \___| 
    */       
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {
            for (Contact estudiante : [SELECT Id, Via_acceso__c, FirstName, LastName, Email, AccountId FROM Contact]) {
                estudiantes.put(estudiante.Email, estudiante);
            }
        }
        if(Trigger.isUpdate) {}
    }

    if(Trigger.isAfter) {
        if(Trigger.isInsert) {

        }
        if(Trigger.isUpdate) {}
    }
         

    /*                         _           _   _            
         _ _ ___ __ ___ _ _ __| |  __ _ __| |_(_)___ _ _  ___
        | '_/ -_) _/ _ \ '_/ _` | / _` / _|  _| / _ \ ' \(_-<
        |_| \___\__\___/_| \__,_| \__,_\__|\__|_\___/_||_/__/
    */
    for(Contact estudiante : (List<Contact>) Trigger.new) {
        if(Trigger.isBefore) {
            if(Trigger.isInsert) {
            }
            if(Trigger.isUpdate) {}
        }
        
        if(Trigger.isAfter) {
            if(Trigger.isInsert) {
                EstudianteTriggerHandler.comprobarSiExisteEstudianteYCrearAdmisionRelacionada(estudiante, estudiantes);
            }
            if(Trigger.isUpdate) {}
        }
    }


    /*
         _         _ _           __ _          
        | |__ _  _| | |__  __ _ / _| |_ ___ _ _
        | '_ \ || | | / / / _` |  _|  _/ -_) '_|
        |_.__/\_,_|_|_\_\ \__,_|_|  \__\___|_| 
    */
     
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {}
        if(Trigger.isUpdate) {}
    }

    if(Trigger.isAfter) {
        if(Trigger.isInsert) {}
        if(Trigger.isUpdate) {}
    }
}

//       
//        .__(.)< (CUAQ)
//         \___)
//  ~~~~~~~~~~~~~~~~~~