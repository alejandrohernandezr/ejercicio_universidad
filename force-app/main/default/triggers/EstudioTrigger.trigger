/**
 * @description       : 
 * @last modified on  : 10-27-2021
 * @last modified by  : Alejandro Hernández
**/
trigger EstudioTrigger on Account (before delete, before insert, before update, after delete, after insert, after update) {

    List<String> valoresPicklistEstudios = new List<String>();
    /*
     _         _ _     _          __             
    | |__ _  _| | |__ | |__  ___ / _|___ _ _ ___ 
    | '_ \ || | | / / | '_ \/ -_)  _/ _ \ '_/ -_)
    |_.__/\_,_|_|_\_\ |_.__/\___|_| \___/_| \___|  
    */        
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {}
        if(Trigger.isUpdate) {}
    }

    if(Trigger.isAfter) {
        if(Trigger.isInsert) {}
        if(Trigger.isUpdate) {}
    }
        
    /*                         _           _   _             
         _ _ ___ __ ___ _ _ __| |  __ _ __| |_(_)___ _ _  ___
        | _/ -_) _/ _ \ '_/ _` | / _` / _|  _| / _ \ ' \(_-<
        |_| \___\__\___/_| \__,_| \__,_\__|\__|_\___/_||_/__/
    */
    for(Account estudio : (List<Account>) Trigger.new) {
        if(Trigger.isBefore) {
            if(Trigger.isInsert) {}
            if(Trigger.isUpdate) {}
        }

        if(Trigger.isAfter) {
            if(Trigger.isInsert) {}
            if(Trigger.isUpdate) {}
        }
    }


    /*
         _         _ _           __ _           
        | |__ _  _| | |__  __ _ / _| |_ ___ _ _ 
        | '_ \ || | | / / / _` |  _|  _/ -_) '_|
        |_.__/\_,_|_|_\_\ \__,_|_|  \__\___|_|  
    */
    
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {}
        if(Trigger.isUpdate) {}
    }

    if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            EstudioTriggerHandler.actualizarValueSetGlobalConNuevoEstudio();
        }
        if(Trigger.isUpdate) {}
    }
}

String query = 'SELECT Admision__r.ContactId, COUNT(Admision__c)e, COUNT(Id)s FROM Solicitud_Documentacion__c WHERE Estado__c = \'Pendiente entrega\' GROUP BY Admision__r.ContactId';
for (AggregateResult agg : [SELECT Admision__r.ContactId FROM Solicitud_Documentacion__c WHERE Estado__c = 'Pendiente entrega' GROUP BY Admision__r.ContactId]) {
    System.debug(JSON.serializePretty(agg));
}


//        
//        .__(.)< (CUAQ)
//         \___)
//  ~~~~~~~~~~~~~~~~~~