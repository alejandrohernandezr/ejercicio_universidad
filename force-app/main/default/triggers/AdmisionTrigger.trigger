/**
 * @description       : 
 * @last modified on  : 10-27-2021
 * @last modified by  : Alejandro Hernández
**/
trigger AdmisionTrigger on Opportunity (before delete, before insert, before update, after delete, after insert, after update) {
 
    Set<Maestro_Documentacion__c> maestrosDocumentacion = new Set<Maestro_Documentacion__c>();
    Map<Id, Account> estudios;
    Map<String, Id> agrupaciones = new Map<String, Id>();
    /*
     _         _ _     _          __            
    | |__ _  _| | |__ | |__  ___ / _|___ _ _ ___
    | '_ \ || | | / / | '_ \/ -_)  _/ _ \ '_/ -_)
    |_.__/\_,_|_|_\_\ |_.__/\___|_| \___/_| \___| 
    */       
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {
            for (Agrupacion_admisiones__c agrupacion : [SELECT Id, Categoria__c FROM Agrupacion_admisiones__c]) {
                agrupaciones.put(agrupacion.Categoria__c, agrupacion.Id);
            }
        }
        if(Trigger.isUpdate) {}
    }

    if(Trigger.isAfter) {
        if(Trigger.isInsert) {

        }
        if(Trigger.isUpdate) {
            estudios = new Map<Id, Account>([SELECT Id, Name, Codigo__c FROM Account]);
            for (Maestro_Documentacion__c maestro : [SELECT Id, Name, Via_acceso__c, Estudio__c FROM Maestro_Documentacion__c]) {
                maestrosDocumentacion.add(maestro);
            }
        }
    }

    /*                         _           _   _            
         _ _ ___ __ ___ _ _ __| |  __ _ __| |_(_)___ _ _  ___
        | '_/ -_) _/ _ \ '_/ _` | / _` / _|  _| / _ \ ' \(_-<
        |_| \___\__\___/_| \__,_| \__,_\__|\__|_\___/_||_/__/
    */


    for(Opportunity admision : (List<Opportunity>) Trigger.new) {
        if(Trigger.isBefore) {
            if(Trigger.isInsert) {
                AdmisionTriggerHandler.asignarAgrupacionAdmisiones(admision, agrupaciones);
            }
            if(Trigger.isUpdate) {}
        }

        if(Trigger.isAfter) {
            if(Trigger.isInsert) {
                
            }
            if(Trigger.isUpdate) {
                if (admision.StageName == AdmisionTriggerHandler.ADMITIDO) {
                    AdmisionTriggerHandler.generarSolicitudDocumentacion(admision, maestrosDocumentacion, estudios);
                }
            }
        }
    }


    /*
         _         _ _           __ _          
        | |__ _  _| | |__  __ _ / _| |_ ___ _ _
        | '_ \ || | | / / / _` |  _|  _/ -_) '_|
        |_.__/\_,_|_|_\_\ \__,_|_|  \__\___|_| 
    */
     
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {}
        if(Trigger.isUpdate) {}
    }

    if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            
        }
        if(Trigger.isUpdate) {}
    }
}

//       
//        .__(.)< (CUAQ)
//         \___)
//  ~~~~~~~~~~~~~~~~~~